import { Component, OnInit, ElementRef, Directive } from '@angular/core';
import { ModalController, AlertController, ToastController } from '@ionic/angular';
import { RestoService } from '../resto.service';
import { Router } from '@angular/router';
import { MenuModalPage } from '../menu-modal/menu-modal.page';
import { HomePage } from '../home/home.page';


@Component({
  selector: 'app-cart-modal',
  templateUrl: './cart-modal.page.html',
  styleUrls: ['./cart-modal.page.scss'],
})
export class CartModalPage implements OnInit {
  isContain:boolean = true;
  cart = [];
  constructor(private modalController:ModalController,private rs:RestoService,private ac:AlertController, private home:HomePage, private toastController:ToastController) { }
  currentactivediskon:any;
  ngOnInit() {
    
    this.cart = JSON.parse(localStorage.getItem('cart'));
    this.rs.getCurrentActiveDiskon().subscribe((data)=>{
      if(data[0] !="tidak ada diskon")
      {
        localStorage.setItem('diskon_berlaku',JSON.stringify(data));
      }
    });
    
    if(this.cart == null)
    {
      this.isContain=false;
    }
    else
    {
      this.isContain=true;
    }
  }

  async alertPesananTelahDimasukkanKeServer() {
    const toast = await this.toastController.create({
      message: 'Pesanan telah diterima',
      duration: 4000
    });
    toast.present();
  }

  async alertKeranjangMasihKosong() {
    const toast = await this.toastController.create({
      message: 'Pesanan telah diterima',
      duration: 4000
    });
    toast.present();
  }
  async alertStockKosong(argNamaMenu:string) {
    const toast = await this.toastController.create({
      message: 'Stock menu ' + argNamaMenu + ' habis. Menu akan otomatis dihapus dari keranjang. Sialhkan melakukan pemesanan menu yang lain.',
      duration: 6000
    });
    toast.present();
  }

  async alertStockTidakCukup(argNamaMenu:string, argSisaStock:string) {
    const toast = await this.toastController.create({
      message: 'Stock menu ' + argNamaMenu + ' tidak cukup. Maksimal pemesanan sebanyak ' + argSisaStock,
      duration: 5000
    });
    toast.present();
  }

  /*async alertPesananTelahDimasukkanKeServer() {
    const alert = await this.ac.create({
      header: 'Pemberitahuan',
      message: 'Pesanan telah diterima',
      buttons: ['OK']
    });

    await alert.present();
  }

  async alertKeranjangMasihKosong() {
    const alert = await this.ac.create({
      header: 'Pemberitahuan',
      message: 'Keranjang anda kosong. Silahkan melakukan pemesanan',
      buttons: ['OK']
    });

    await alert.present();
  }

  async alertStockKosong(argNamaMenu:string) {
    const alert = await this.ac.create({
      header: 'Pemberitahuan',
      message: 'Stock menu ' + argNamaMenu + ' habis. Menu akan otomatis dihapus dari keranjang. Sialhkan melakukan pemesanan menu yang lain.',
      buttons: ['OK']
    });

    await alert.present();
  }

  async alertStockTidakCukup(argNamaMenu:string, argSisaStock:string) {
    const alert = await this.ac.create({
      header: 'Pemberitahuan',
      message: 'Stock menu ' + argNamaMenu + ' tidak cukup. Maksimal pemesanan sebanyak ' + argSisaStock, 
      buttons: ['OK']
    });

    await alert.present();
  }*/

  async menuModal(argIdMenu:string){
    const modal = await this.modalController.create({
      component:MenuModalPage,
      componentProps: { idMenu : argIdMenu}
    });

    modal.onDidDismiss().then(
      (data)=>{
        this.cart = JSON.parse(localStorage.getItem('cart'));
        this.home.itemInCart = JSON.parse(localStorage.getItem('cart'));
      }
    )
    return await modal.present();
  }

  closeModal(){
    localStorage.removeItem('diskon_berlaku');
    this.modalController.dismiss()
  }

  hitungSubTotal(argJumlahOrder:number, argHargaSatuan:number):number
  {
    
    let subTotal = Number(argJumlahOrder) * Number(argHargaSatuan);
    return Math.trunc(subTotal);
  }

  hitungSubTotalSetelahDiskon(argJumlahOrder:number, argHargaSatuan:number):number
  {
    this.currentactivediskon = JSON.parse(localStorage.getItem('diskon_berlaku'));
    let subTotal = Number(argJumlahOrder) * Number(argHargaSatuan);
    let subTotalSetelahDiskon:number = subTotal - (subTotal * (this.currentactivediskon[0]['besar_diskon']/100));
  
    
    return Math.trunc(subTotalSetelahDiskon);
  }

  hitungTotal():number
  {
    let total = 0;
    
    if(JSON.parse(localStorage.getItem('diskon_berlaku')) != null)
    {
      this.currentactivediskon = JSON.parse(localStorage.getItem('diskon_berlaku'));
    } 
    if(this.cart!=null)
    {
      for(let i =0; i< this.cart.length;i++)
      {
        let tidakAdaDiskon = false;
        if(this.cart[i]['jenis']=="makanan")
        {
          if(this.currentactivediskon[0]['is_makanan_diskon'] == 1 && this.currentactivediskon != "tidak ada diskon")
          {
            total += this.hitungSubTotalSetelahDiskon(this.cart[i]['jumlahOrder'], this.cart[i]['hargaSatuan']);
          }
          else {
            tidakAdaDiskon = true;
          }
        }
        if(this.cart[i]['jenis']=="minuman")
        {
          if(this.currentactivediskon[0]['is_minuman_diskon'] == 1 && this.currentactivediskon != "tidak ada diskon")
          {
            total += this.hitungSubTotalSetelahDiskon(this.cart[i]['jumlahOrder'], this.cart[i]['hargaSatuan']);
          }
          else
          {
            tidakAdaDiskon = true;
          }
        }

        if(tidakAdaDiskon == true)
        {
          total += this.hitungSubTotal(this.cart[i]['jumlahOrder'],this.cart[i]['hargaSatuan']);
        }

      }
    }
    return Math.trunc(total);
  }
  removeOrder(argIndex:number)
  {
    this.cart.splice(argIndex,1);
    localStorage.removeItem('cart');
    localStorage.setItem('cart',JSON.stringify(this.cart));
  }

  is_diskon(argJenis:string):boolean
  {
    if(JSON.parse(localStorage.getItem('diskon_berlaku')) != "tidak ada diskon")
    {
      this.currentactivediskon = JSON.parse(localStorage.getItem('diskon_berlaku'));
      
      if(argJenis == "makanan")
      {
        if(this.currentactivediskon[0]['is_makanan_diskon'] == 1)
        {
          return true; 
        }
      }
      else if(argJenis == "minuman")
      {
        if(this.currentactivediskon[0]['is_minuman_diskon'] == 1)
        {
          return true;
        }
      }
      
    }

    return false;
  }
  order()
  {
    
    if(localStorage.getItem('cart')==null)
    {
      this.alertKeranjangMasihKosong();
    }
    else if(JSON.parse(localStorage.getItem('cart')).length ==0)
    {
      this.alertKeranjangMasihKosong();
    }
    else
    {
      
      this.rs.insertToNota(localStorage.getItem('cart'),localStorage.getItem('username'),localStorage.getItem('nomor_meja'),localStorage.getItem('diskon_berlaku')).subscribe(
        (data)=>{
          console.log(data);
          if(data != "success")
          {
            
            for(let i=0; i<data.length;i++)
            {
              if(data[i]['stock'] == 0)
              {
                this.alertStockKosong(data[i]['nama_menu']);
                for(let j =0; j<this.cart.length;j++)
                {
                  if(this.cart[j]['nama_menu'] == data[i]['nama_menu'])
                  {
                    this.cart.splice(j,1);
                  }
                }
              }
              else
              {
                this.alertStockTidakCukup(data[i]['nama_menu'],data[i]['stock']);
                
              }  
            }
            localStorage.removeItem('cart');
            localStorage.setItem('cart',JSON.stringify(this.cart));
          }
          else
          {
            this.alertPesananTelahDimasukkanKeServer();
            localStorage.removeItem('cart');
            this.closeModal();
          }
        }
      )
    }
  }
}
