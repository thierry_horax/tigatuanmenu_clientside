import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { IonicSwipeAllModule } from 'ionic-swipe-all';
import { IonicModule, IonicRouteStrategy, IonBadge } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { MenuModalPage } from './menu-modal/menu-modal.page';
import { AlertDiskonModalPage } from './alert-diskon-modal/alert-diskon-modal.page';
import { SwiperModule } from 'ngx-swiper-wrapper';
import { SWIPER_CONFIG } from 'ngx-swiper-wrapper';
import { SwiperConfigInterface } from 'ngx-swiper-wrapper';
import { CartModalPage } from './cart-modal/cart-modal.page';
import { HomePage } from './home/home.page';
import { FormsModule } from '@angular/forms';
import { HistoryDetailPage } from './history-detail/history-detail.page';
import { DatePipe } from '@angular/common';
const DEFAULT_SWIPER_CONFIG: SwiperConfigInterface = {
  direction: 'vertical',
  slidesPerView: 'auto'
};

@NgModule({
  declarations: [AppComponent],
  entryComponents: [MenuModalPage,CartModalPage,AlertDiskonModalPage,HistoryDetailPage],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    { provide: SWIPER_CONFIG, useValue: DEFAULT_SWIPER_CONFIG },
    HomePage,
    DatePipe

    
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
