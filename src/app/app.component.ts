import { Component, OnInit } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent  {
  userlogin="";
  public appPagesNotLogin = [
    {
      title: 'Home',
      url: '/home',
      icon: 'home'
    }
    ,
    {
      title: 'Masuk',
      url: '/login-page',
      icon: 'log-in'
    }
    
    
  ];

  public appPagesAlreadyLogin = [
    {
      title: 'Home',
      url: '/home',
      icon: 'home'
    }
    ,
    {
      title: 'Pesanan',
      url: '/pesanan',
      icon: 'paper'
    }
    ,
    {
      title: 'Keluar',
      url: '/logout-page',
      icon: 'log-out'
    }
    
  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.isLogin();
    });
  }

  isLogin():boolean{
    let isLogin = false;
    if(localStorage.getItem('username')!=null)
    {
      this.userlogin = localStorage.getItem('username');
      isLogin = true;
    }
   
    return isLogin;
  }
}
