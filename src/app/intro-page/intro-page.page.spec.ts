import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntroPagePage } from './intro-page.page';

describe('IntroPagePage', () => {
  let component: IntroPagePage;
  let fixture: ComponentFixture<IntroPagePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntroPagePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntroPagePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
