import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { AlertDiskonModalPage } from './alert-diskon-modal.page';

const routes: Routes = [
  {
    path: '',
    component: AlertDiskonModalPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [AlertDiskonModalPage]
})
export class AlertDiskonModalPageModule {}
