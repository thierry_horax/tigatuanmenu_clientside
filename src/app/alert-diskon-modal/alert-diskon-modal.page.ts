import { Component, OnInit } from '@angular/core';
import { RestoService } from '../resto.service';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-alert-diskon-modal',
  templateUrl: './alert-diskon-modal.page.html',
  styleUrls: ['./alert-diskon-modal.page.scss'],
})
export class AlertDiskonModalPage implements OnInit {
  judul_diskon ="";
  keterangan_diskon = "";
  masa_berlaku = "";
  ext="";
  constructor(public rs:RestoService, private modal:ModalController) { }

  ngOnInit() {
    this.rs.getCurrentActiveDiskon().subscribe((data)=>{
      if(data[0] !="tidak ada diskon")
      {
        this.judul_diskon = data[0]['judul_diskon'];
        this.keterangan_diskon = data[0]['keterangan_diskon'];
        this.masa_berlaku = data[0]['waktu_berlaku_diskon'] + " hingga " + data[0]['waktu_berakhir_diskon'];
        this.ext = data[0]['ext'];
      }
    });
  }
  close(){
    this.modal.dismiss();
  }
}
