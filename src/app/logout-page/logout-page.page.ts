import { Component, OnInit } from '@angular/core';
import { Route, ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-logout-page',
  templateUrl: './logout-page.page.html',
  styleUrls: ['./logout-page.page.scss'],
})
export class LogoutPagePage implements OnInit {

  constructor(private router:Router) { }

  ngOnInit() {
    localStorage.clear();
    this.router.navigateByUrl('');
  }

}
