import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { observable, Observable } from 'rxjs';
import { HttpParams } from '@angular/common/http';
import { SafeResourceUrl } from '@angular/platform-browser';
const url_file:string = "https://tigatuanonline.000webhostapp.com";
//const url_file:string = "http://localhost";
//const url_file:string = "http://keprak.kinkin.moe/restomenu_server/public";
//const url_file:string = "http://192.168.100.13:80";
@Injectable({
  providedIn: 'root'
})

export class RestoService {
  
  constructor(private hc:HttpClient) { }
  
  
  login(argUsername:string):Observable<string>{
    let parameter = new HttpParams();
    parameter = parameter.set('username',argUsername);
    return this.hc.post<string>(url_file +'/resto_menu/login.php',parameter);
  }

  checkStockMenu(argIdMenu:string):Observable<string>{
    let parameter = new HttpParams();
    parameter = parameter.set('idmenu',argIdMenu);
    return this.hc.post<string>(url_file +'/resto_menu/checkstockmenu.php',parameter);
  }

  getAllCategory():Observable<string>{
    let parameter = new HttpParams();
    return this.hc.post<string>(url_file +'/resto_menu/getallcategory.php',parameter);
  }

  getCurrentActiveDiskon():Observable<string>{
    let paramter = new HttpParams();
    return this.hc.post<string>(url_file +'/resto_menu/getcurrentactivediskon.php',paramter);
  }

  getAllMenu():Observable<string>{
    let parameter = new HttpParams();
    return this.hc.post<string>(url_file +'/resto_menu/getallmenu.php',parameter);
  }

  getDetailOfMenu(argIdMenu:string):Observable<string>{
    let parameter = new HttpParams();
    parameter = parameter.set('idmenu',argIdMenu);
    return this.hc.post<string>(url_file +'/resto_menu/getdetailofmenu.php',parameter);
  }

  getMenuByCategoryName(argIdKategori:string):Observable<string>{
    let parameter = new HttpParams();
    parameter = parameter.set('idkategori',argIdKategori);
    return this.hc.post<string>(url_file +'/resto_menu/getmenubycategoryname.php',parameter);
  }

  getMenuByMenuName(argNamaMenu:string):Observable<string>{
    let parameter = new HttpParams();
    parameter = parameter.set('nama_menu',argNamaMenu);
    return this.hc.post<string>(url_file +'/resto_menu/getmenubymenuname.php',parameter);
  }

  getInformationLoginUser(argUsername:string):Observable<string>{
    let parameter = new HttpParams();
    parameter = parameter.set('username',argUsername);
    return this.hc.post<string>(url_file +'/resto_menu/getinformationloginuser.php',parameter);
  }

  getHistory(argUsername:string):Observable<string>{
    let parameter= new HttpParams();
    parameter = parameter.set('username',argUsername);
    return this.hc.post<string>(url_file + "/resto_menu/getHistory.php",parameter);
  }

  getHistoryDetail(argNomorNota:string):Observable<string>{
    let parameter= new HttpParams();
    parameter = parameter.set('nomor_nota',argNomorNota);
    return this.hc.post<string>(url_file + "/resto_menu/getHistoryDetail.php",parameter);
  }

  getPesananYangDISiapkan(argUsername:string):Observable<string>{
    let parameter= new HttpParams();
    parameter = parameter.set('username',argUsername);
    return this.hc.post<string>(url_file + "/resto_menu/getPesananYangDisiapkan.php",parameter);
  }

  insertToNota(argCart:string, argUsername:string, argNomorMeja:string, argDiskon:string)
  {
    let parameter = new HttpParams();
    parameter = parameter.set('cart',argCart);
    parameter = parameter.set('nomor_meja',argNomorMeja);
    parameter = parameter.set('username',argUsername);
    parameter = parameter.set('diskon',argDiskon);
    return this.hc.post<string>(url_file +'/resto_menu/inserttonota.php',parameter);
  }

  autoFillKeteranganmenu(argUsername:string, argIdMenu:string):Observable<string>
  {
    let parameter = new HttpParams();
    parameter = parameter.set('username',argUsername);
    parameter = parameter.set('idmenu',argIdMenu);
    return this.hc.post<string>(url_file + '/resto_menu/autofillketeranganmenu.php',parameter);
  }
}

