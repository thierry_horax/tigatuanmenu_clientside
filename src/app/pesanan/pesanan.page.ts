import { Component, OnInit } from '@angular/core';
import { RestoService } from '../resto.service';
import { HistoryDetailPage } from '../history-detail/history-detail.page';
import { ModalController, NavController } from '@ionic/angular';
import { Router } from '@angular/router';
import { DatePipe } from '@angular/common';
@Component({
  selector: 'app-pesanan',
  templateUrl: './pesanan.page.html',
  styleUrls: ['./pesanan.page.scss'],
})
export class PesananPage implements OnInit {
  username:string ="";
  isEmpty = true;
  display = "";
  totalPesananYangSedangDisiapkan= 0;
  constructor(public rs:RestoService, private modalController:ModalController, private router:Router,private datePipe: DatePipe) { }

  ngOnInit() {
    this.username = localStorage.getItem('username');
        
    this.rs.getPesananYangDISiapkan(this.username).subscribe((data)=>{
      this.totalPesananYangSedangDisiapkan=data.length;
      
    });
  }

  transformDate(argDate):string{
    let oldDate = new Date(argDate);
    let newDate = "";
    newDate = this.datePipe.transform(oldDate, 'dd-MM-yyyy h:mm:ss a');
    return newDate;
  }

  segmentChanged(ev: any) {
    let segment = ev.target.value;
    this.display = null;
    this.getListPesananBerdasarkanSegmen(segment);

    
    this.rs.getPesananYangDISiapkan(this.username).subscribe((data)=>{
      this.totalPesananYangSedangDisiapkan=data.length;
      
    });

  }

  getListPesananBerdasarkanSegmen(argSegment)
  {
    if(argSegment == "riwayat")
    {
      this.rs.getHistory(this.username).subscribe((data)=>{
        this.display = data;
      });
    }
    else if(argSegment == "proses")
    {
      this.rs.getPesananYangDISiapkan(this.username).subscribe((data)=>{
        this.display=data;
        
      });
    }
  }

  countJumlahPesananYangSedangDisiapkan(argData):number{
    let total = argData.length;
    
    return total;
  }
  async historyDetail(argNomorNota:string){
    const modal = await this.modalController.create({
      component: HistoryDetailPage,
      componentProps: {nomor_nota : argNomorNota}
    })

    return await modal.present();
  }

  closeModal(){
    this.router.navigateByUrl('');
  }

}
