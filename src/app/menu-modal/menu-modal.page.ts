import { Component, Input , OnInit} from '@angular/core';
import { NavParams, ToastController } from '@ionic/angular';
import { ModalController } from '@ionic/angular';
import { RestoService } from '../resto.service';
import { AlertController } from '@ionic/angular';
import { CartModalPage } from '../cart-modal/cart-modal.page';
import { Router } from '@angular/router';
@Component({
  selector: 'app-menu-modal',
  templateUrl: './menu-modal.page.html',
  styleUrls: ['./menu-modal.page.scss'],
})
export class MenuModalPage implements OnInit{
  totalItemInCart=0;
  itemInCart = [];
  url_asset = "";
  currentPermintaanKhusus = "";
  isChange:boolean;
  isRun:boolean = false;
  idMenu:string = "";
  permintaanKhusus = "";
	jumlahOrder:number = 1;
  totalOrder:number = 0;
  hargaSatuan:number =0;
  namaMenu:string = "";
  jenis:string="";
  keteranganMenu:string="";
  isContain:boolean = false;
  isExist:boolean = false;
  indexItem = 0;
  extension:string ="";
  daftarPesanan = [];
  autofill_referensi = "";
  constructor(public modalController:ModalController,public navParams:NavParams,private modal:ModalController,private rs:RestoService,private ac:AlertController,private toastController:ToastController,public router:Router) { }

  @Input() value:string;

  closeModal(){
    this.modal.dismiss()
  }
  ngOnInit(){
    this.url_asset = localStorage.getItem('url_asset');
    this.itemInCart= JSON.parse(localStorage.getItem('cart'))
    this.countItemInCart();

    this.isChange = false;
    this.isRun=true;
    this.daftarPesanan = JSON.parse(localStorage.getItem('cart'));
    
    
    this.rs.getDetailOfMenu(this.navParams.get('idMenu')).subscribe(
      (data)=>{
        this.keteranganMenu = data['keterangan_menu'];
        this.idMenu = data['idMenu'];
        this.hargaSatuan = data['harga'];
        this.namaMenu = data['nama_menu'];
        this.jenis = data['jenis'];
        this.extension = data['ext'];
      }
    )
    this.isPesananSudahAdaDiKeranjang();
    this.rs.autoFillKeteranganmenu(localStorage.getItem('username'), this.idMenu).subscribe((data)=>{
      this.autofill_referensi = data;
    });
  }

  async openCart(){
    const modal = await this.modalController.create({
      component: CartModalPage,
    })

    modal.onDidDismiss().then(
      (data)=>{
        this.itemInCart = JSON.parse(localStorage.getItem('cart'));
        this.countItemInCart();
      }
    )

    return await modal.present();
  }

  countItemInCart(){
    if(this.itemInCart == null)
    {
      this.totalItemInCart = 0;
    }
    else
    {
      this.totalItemInCart = this.itemInCart.length;
    }
  }

  isLogin():boolean{
    let isLogin = false;
    if(localStorage.getItem('username')!=null)
    {
      isLogin = true;
    }
    return isLogin;
  }

  getCurrentPermintaanKhususDariCart():string{
    let permintaanKhusus = "";
    if(this.daftarPesanan!=null)
    {
      for(let i = 0; i<this.daftarPesanan.length;i++)
        {
          if(this.daftarPesanan[i]['nama_menu']== this.namaMenu)
          {
          
            permintaanKhusus = this.daftarPesanan[i]['permintaan_khusus'];
            
            break;
          }
        }
    }
    
    return permintaanKhusus;
  }

  getCurrentJumlahOrderDiCart():number
  {
    if(this.daftarPesanan!=null)
    {
      for(let i = 0; i<this.daftarPesanan.length;i++)
      {
        if(this.daftarPesanan[i]['nama_menu']== this.namaMenu)
        {
          this.jumlahOrder = this.daftarPesanan[i]['jumlahOrder'];
        }
      }
    }
    return this.jumlahOrder;
  }
  
  isPesananSudahAdaDiKeranjang():boolean
  {
    if(this.daftarPesanan!=null)
    {
      for(let i = 0; i < this.daftarPesanan.length;i++) 
      {
        if(this.daftarPesanan[i]['nama_menu']==this.namaMenu)
        {
          this.indexItem = i;
          this.isContain = true;
          break;
          
        }
        else
        {
          this.isContain=false;
        }
      }
    }
      return this.isContain;
  }

  getPermintaanKhusus(event:any)
  {
    this.permintaanKhusus = event.target.value;    
    this.isChange = true;
  }

  removeOrder(){
    this.isPesananSudahAdaDiKeranjang();
    if(this.jumlahOrder<=1)
    {
      this.jumlahOrder=1;
    }
    else
    {
      this.jumlahOrder--;
    }
    if(this.isContain==true)
    {
      this.daftarPesanan[this.indexItem]['jumlahOrder'] = this.jumlahOrder;
    }
    this.getCurrentJumlahOrderDiCart();
    this.getTotal();
  }

  addOrder(){
    this.isPesananSudahAdaDiKeranjang();
    this.jumlahOrder++;
    if(this.isContain==true)
    {
      this.daftarPesanan[this.indexItem]['jumlahOrder'] = this.jumlahOrder;
    }
    this.getCurrentJumlahOrderDiCart();
    this.getTotal();
  }
  getTotal():number{
  	return this.jumlahOrder * this.hargaSatuan;
  }


  addToCart()
  {
    if(this.daftarPesanan == null)
    {
      this.daftarPesanan = [{'idmenu' : this.navParams.get('idMenu'),'nama_menu':this.namaMenu , 'jumlahOrder': this.jumlahOrder , 'hargaSatuan' : this.hargaSatuan, 'permintaan_khusus' : this.autofill_referensi, 'jenis' : this.jenis}];
      this.alertMenuDitambahKekeranjang();
    }
    else
    {
      this.isPesananSudahAdaDiKeranjang();
      if(this.isContain==false)
      {
        this.alertMenuDitambahKekeranjang();
        this.daftarPesanan[this.daftarPesanan.length] = {'idmenu' :this.navParams.get('idMenu'),'nama_menu':this.namaMenu , 'jumlahOrder': this.jumlahOrder, 'hargaSatuan' : this.hargaSatuan, 'permintaan_khusus' : this.autofill_referensi, 'jenis' : this.jenis};
      }
      else
      {
        if(this.permintaanKhusus != "" || this.isChange == true)
        {
          this.daftarPesanan[this.indexItem]['permintaan_khusus'] = this.permintaanKhusus;
        }
        this.alertPerubahanPesananTelahDiSimpan();
      }
    }
    localStorage.setItem('cart',JSON.stringify(this.daftarPesanan));
    this.closeModal();
  }

  async testingAutoFill(argReferensi) {
    const alert = await this.ac.create({
      header: 'Pemberitahuan',
      message: argReferensi ,
      
      buttons: ['OK']
    });

    await alert.present();
  }
/*
  async alertPerubahanPesananTelahDiSimpan() {
    const alert = await this.ac.create({
      header: 'Pemberitahuan',
      message: 'Perubahan pesanan telah disimpan',
      buttons: ['OK']
    });

    await alert.present();
  }*/

  async alertPerubahanPesananTelahDiSimpan() {
    const toast = await this.toastController.create({
      message: 'Perubahan pesanan telah disimpan',
      duration: 3000
    });
    toast.present();
  }

  async alertMenuDitambahKekeranjang() {
    const toast = await this.toastController.create({
      message: 'Pesanan telah ditambahkan ke keranjang',
      duration: 3000
    });
    toast.present();
  }
}
