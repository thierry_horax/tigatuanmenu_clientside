import { Component,OnInit } from '@angular/core';
import { ModalController, Platform } from '@ionic/angular';
import { MenuModalPage } from '../menu-modal/menu-modal.page';
import { RestoService } from '../resto.service';
import { CartModalPage } from '../cart-modal/cart-modal.page';
import { AlertDiskonModalPage } from '../alert-diskon-modal/alert-diskon-modal.page';
import { PlatformLocation } from '@angular/common';
import { SplashscreenPage } from '../splashscreen/splashscreen.page';
import { Navigation } from 'selenium-webdriver';
import { Router } from '@angular/router';
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  public url_asset = "./assets/images_menu/";
  //public url_asset = "https://tigatuanonline.000webhostapp.com/images_menu/";
  //public url_asset = "http://192.168.100.13/restomenu_server/resources/assets/images_menu/";
  public daftarMenu="";
  public daftarKategori = "";
  stockKosong = true;
  isSortByCategory = false;
  menu = [];
  keyword = "";
  itemInCart = [];
  totalItemInCart=0;
  isSearchBarNotEmpty=false;
  private isDisplay;
  
  constructor(public modalController:ModalController, public rs:RestoService,private pl:PlatformLocation,public platform:Platform, private router:Router) {
    
  }


  ngOnInit(){

    localStorage.setItem('url_asset',this.url_asset);
    if(localStorage.getItem('username')==null)
    {
      this.isDisplay=true;
      this.rs.getCurrentActiveDiskon().subscribe((data)=>{
        if(data !="tidak ada diskon")
        {
          this.displayDiskon();
        }
      });
    }
    
    this.getAllCategory();
    this.itemInCart= JSON.parse(localStorage.getItem('cart'))
    this.countItemInCart();
    if(this.keyword=="")
    {
      if(this.isSortByCategory==false)
      {
        this.getAllMenu();
      }
      else
      {
        
      }
    }
    else
    {
      this.getSearchValue();
    }
    
  }

  autoFill(argIdMenu):string{
    let username = localStorage.getItem('username');
    let idmenu = argIdMenu;
    let referensi = "";
    this.rs.autoFillKeteranganmenu(username, idmenu).subscribe((data)=>{
      //referensi = data;
       return data;
    });
    return "";
  }

  test(argIdMenu):string {
    return this.autoFill(argIdMenu);
  }

  async menuModal(argIdMenu:string){
    const modal = await this.modalController.create({
      component: MenuModalPage,
      componentProps: {idMenu : argIdMenu, referensi: this.test(argIdMenu)},
      backdropDismiss:true,
    })

    modal.onDidDismiss().then(
      (data)=>{
        this.itemInCart = JSON.parse(localStorage.getItem('cart'));
        this.countItemInCart();
      }
    )
    return await modal.present();
  }

  //cssClass ada di app.scss bukan di alert-diskon-modal.page.scss / home.page.scss
  async displayDiskon(){
    let isDisplay = true;
    if(isDisplay == true)
    {
      const modal = await this.modalController.create({
        component: AlertDiskonModalPage,
        cssClass : 'modal-page',
        backdropDismiss:true, 
      })
      isDisplay=false;
      return await modal.present();
    }
  }

  async openCart(){
    const modal = await this.modalController.create({
      component: CartModalPage,
      backdropDismiss:true,
    })

    modal.onDidDismiss().then(
      (data)=>{
        this.itemInCart = JSON.parse(localStorage.getItem('cart'));
        this.countItemInCart();
      }
    )

    return await modal.present();
  }

  countItemInCart(){
    if(this.itemInCart == null)
    {
      this.totalItemInCart = 0;
    }
    else
    {
      this.totalItemInCart = this.itemInCart.length;
    }
  }


  isLogin():boolean{
    let islogin = true;
    if(localStorage.getItem('username')==null)
    {
      islogin= false;
    }
    return islogin;
  }

  getAllCategory(){
    this.rs.getAllCategory().subscribe(
      (data)=>{
        this.daftarKategori = data;
      }
    )
  }

  getAllMenu(){
    this.rs.getAllMenu().subscribe(
      (data)=>{
        this.daftarMenu = data;
        this.isSortByCategory=false;
      }
    )
  }

  getValue(event:any)
  {
    this.keyword= event.target.value;
    if(this.keyword != "" || this.keyword =="")
    {
      this.isSearchBarNotEmpty=true;
    }
    this.rs.getMenuByMenuName(this.keyword).subscribe(
        (data)=>{
          this.daftarMenu = data;
          this.ngOnInit();
        }
      )
  }

  getMenuByCategory(event:any){
    let argIdKategori = event.target.value;
    console.log(argIdKategori);
    if(argIdKategori=="Semua Menu")
    {
      this.getAllMenu();
    }
    else
    {
      this.rs.getMenuByCategoryName(argIdKategori).subscribe(
      (data) =>{
        this.daftarMenu = data;
        this.isSortByCategory=true;
        this.ngOnInit();
      }
    )
    }
  }

  getSearchValue()
  {
      
  }

  isStockKosong(argStock:number):boolean
  {
    let isKosong = false;
    if(argStock == 0)
    {
      isKosong=true;
    }
    return isKosong;
  }
}
