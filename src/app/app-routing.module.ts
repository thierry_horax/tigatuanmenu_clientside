import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: './home/home.module#HomePageModule'
  },
  {
    path: 'list',
    loadChildren: './list/list.module#ListPageModule'
  },
  { path: 'menu-modal', loadChildren: './menu-modal/menu-modal.module#MenuModalPageModule' },
  { path: 'cart-modal', loadChildren: './cart-modal/cart-modal.module#CartModalPageModule' },
  { path: 'intro-page', loadChildren: './intro-page/intro-page.module#IntroPagePageModule' },
  { path: 'intro-page', loadChildren: './intro-page/intro-page.module#IntroPagePageModule' },
  { path: 'login-page', loadChildren: './login-page/login-page.module#LoginPagePageModule' },
  { path: 'logout-page', loadChildren: './logout-page/logout-page.module#LogoutPagePageModule' },
  { path: 'alert-diskon-modal', loadChildren: './alert-diskon-modal/alert-diskon-modal.module#AlertDiskonModalPageModule' },
  { path: 'pesanan', loadChildren: './pesanan/pesanan.module#PesananPageModule' },
  { path: 'history-detail', loadChildren: './history-detail/history-detail.module#HistoryDetailPageModule' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
