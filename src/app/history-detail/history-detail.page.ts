import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { RestoService } from '../resto.service';
import { DatePipe} from '@angular/common';

@Component({
  selector: 'app-history-detail',
  templateUrl: './history-detail.page.html',
  styleUrls: ['./history-detail.page.scss'],
})
export class HistoryDetailPage implements OnInit {
  nomor_nota = "";
  nomor_meja = "";
  tanggal_transaksi = "";
  status = "";
  daftarOrder ="";
  pesananDetail = "";
  constructor(private modalController:ModalController, public rs:RestoService, private navParams:NavParams, private datePipe:DatePipe) { }

  ngOnInit() {

    this.rs.getHistoryDetail(this.navParams.get('nomor_nota')).subscribe((data)=>{
      this.daftarOrder = data;
      this.nomor_meja = data[0]['nomor_meja'];
      this.nomor_nota = data[0]['nomor_nota'];
      this.tanggal_transaksi = this.transformDate(data[0]['tanggal_transaksi']);
      this.status = data[0]['status'];
    });
    this.rs.getHistoryDetail(this.navParams.get('nomor_nota')).subscribe((data)=>{
      this.pesananDetail = data;
    });
    
  }


  transformDate(argDate):string{
    let oldDate = new Date(argDate);
    let newDate = "";
    newDate = this.datePipe.transform(oldDate, 'dd-MM-yyyy h:mm:ss a');
    return newDate;
  }

  countTotal(argPesananDetail):number{
    let total:number =0;
    for(let i=0; i< argPesananDetail.length;i++)
    {
      total+= Number(argPesananDetail[i]['harga_sesudah_diskon']);
    }
    return total;
  }

  closeModal(){
    this.modalController.dismiss()
  }

}
