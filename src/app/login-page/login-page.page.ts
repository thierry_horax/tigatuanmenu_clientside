import { Component, OnInit } from '@angular/core';
import { RestoService } from '../resto.service';
import { AlertController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.page.html',
  styleUrls: ['./login-page.page.scss'],
})
export class LoginPagePage implements OnInit {

  constructor(private rs:RestoService,private ac:AlertController,private router:Router) { }

  username:string = "";
  nomor_meja:string = "";
  ngOnInit() {
    if(localStorage.getItem('username') != null)
    {
      this.router.navigateByUrl('/home');
    }
  }

  getNomorMeja(event:any){
    this.nomor_meja = event.target.value;
  }

  getUsername(event:any){
    this.username= event.target.value;
  }

  async alertNomorMejaKosong() {
    const alert = await this.ac.create({
      header: 'Pemberitahuan',
      message: 'Silahkan isi nomor meja.',
      buttons: ['OK']
    });

    await alert.present();
  }

  async alertUsernameSalah() {
    const alert = await this.ac.create({
      header: 'Pemberitahuan',
      message: 'Username salah. Silahkan periksa kembali',
      buttons: ['OK']
    });

    await alert.present();
  }

  login(){
    if(this.nomor_meja == "")
    {
      this.alertNomorMejaKosong();
    }
    else
    {
      this.rs.login(this.username).subscribe(
        (data)=>{
          if(data ==null)
          {
            this.alertUsernameSalah();
          }
          else
          {
            localStorage.setItem('nomor_meja',this.nomor_meja);
            localStorage.setItem('username',data);
            this.router.navigateByUrl('');
          }
          
        }
      )
    }
  }
}
